FROM php:8.3-alpine3.20

MAINTAINER "Kais Selmi" <selmi.kaies@gmail.com>

# Set correct environment variables for composer.
ENV COMPOSER_MEMORY_LIMIT=-1 COMPOSER_VENDOR_DIR=vendor COMPOSER_CACHE_DIR=/tmp/.composer COMPOSER_ALLOW_SUPERUSER=1

# Install the latest version of composer.
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

RUN apk add --no-cache --virtual .tools git openssh && \
    apk add --no-cache --virtual .dev-packages freetype-dev && \
    apk add --no-cache --virtual .packages zip bzip2 && \
    apk add --no-cache --virtual .build-deps zlib-dev libzip-dev oniguruma-dev && \
    docker-php-ext-install zip pcntl bcmath pdo pdo_mysql mbstring sockets && \
    docker-php-ext-enable zip pcntl bcmath pdo pdo_mysql mbstring sockets && \
    apk del --purge .dev-packages .packages && \
    rm -rf /var/lib/apt/lists/* /var/cache/apk/* /tmp/*

VOLUME ["/var/"]
WORKDIR /var/www

ENTRYPOINT ["composer"]
